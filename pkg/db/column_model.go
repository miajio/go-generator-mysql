package db

import (
	"fmt"
	"strings"
)

// ColumnModel 字段列参数详情
type ColumnModel struct {
	Name       string            // 字段名
	Type       string            // 字段类型
	IsPoint    bool              // 是否为指针类型
	Tag        map[string]string // 标签
	Annotation *string           // 注释
}

// ToStructColumn 字段列详情转结构体字段字符串
func (d *ColumnModel) ToStructColumn() string {
	params := make([]string, 0)

	params = append(params, d.Name)

	tp := d.Type

	if d.IsPoint && d.Type != "[]byte" {
		tp = "*" + tp
	}
	params = append(params, tp)

	tags := make([]string, 0)
	for k, v := range d.Tag {
		tags = append(tags, fmt.Sprintf("%s:\"%s\"", k, v))
	}
	tag := "`" + strings.Join(tags, " ") + "`"

	params = append(params, tag)

	if d.Annotation != nil && *d.Annotation != "" {
		params = append(params, "//")
		params = append(params, *d.Annotation)
	}

	return strings.Join(params, " ")
}

const (
	funcTemplate = ``
)

func (d *ColumnModel) GeneratorFunc() string {
	return ""
}
