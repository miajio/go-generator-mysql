# go-generator-mysql

## 介绍
仿java mybatis generator 代码生成工具

go语言版本 >= 1.18

### 已完成工作
连接数据库后依据数据库表名生成结构体

### 需要完成工作
生成结构体后生成结构体快捷操作Example

#### Example结构体需生成
findById key = ?

page limit (?, ?)

#### 基于结构体对应参数生成
EqualTo(val interface{}) param = ?

NotEqualTo(val interface{}) param <> ?

GreaterThan(val interface{}) param > ?

GreaterThanOrEqualTo(val interface{}) param >= ?

LessThan(val interface{}) param < ?

LessThanOrEqualTo(val interface{}) param <= ?

Like(val interface{}) param like ?

NotLike(val interface{}) param not like ?

In(val interface{}) param in (?)

NotIn(val interface{}) param not in (?)

Between(a, b interface{}) param between (?, ?)

NotBetween(a, b interface{}) param not between (?, ?)

IsNull param is null

IsNotNull param not null