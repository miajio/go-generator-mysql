package db

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var LinkText string

func DBConnect() (*sqlx.DB, error) {
	return sqlx.Connect("mysql", LinkText)
}
