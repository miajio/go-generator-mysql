package generate_test

import (
	"fmt"
	"testing"

	"gitee.com/miajio/go-generator-mysql/pkg/generate"
)

var defaultParam = `[{"Field":"str","Type":"varchar(32)","Null":"NO","Key":"PRI","Default":null,"Comment":"ID"},{"Field":"str_text","Type":"text","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"str_longtext","Type":"longtext","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"str_mediumblob","Type":"mediumblob","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"str_varbinary","Type":"varbinary(10)","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"s","Type":"binary(10)","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"str_longblob","Type":"longblob","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"str_blob","Type":"blob","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"int_line","Type":"int","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"u_int_line","Type":"int unsigned","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"integer_line","Type":"int","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"u_integer_line","Type":"int unsigned","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"big_line","Type":"bigint","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"long_line","Type":"mediumtext","Null":"YES","Key":"","Default":null,"Comment":""},{"Field":"u_big_line","Type":"bigint unsigned","Null":"YES","Key":"","Default":null,"Comment":""}]`

func TestGen(t *testing.T) {
	// var columns []db.DescTable
	// if err := json.Unmarshal([]byte(defaultParam), &columns); err != nil {
	// 	fmt.Printf("err %v", err)
	// }

	// fmt.Println(generate.GeneratorStruct("gen_test", columns, true))
}

func TestDBGen(t *testing.T) {
	// db.LinkText = "root:123456@tcp(localhost:3306)/test?charset=utf8mb4&parseTime=True"
	// table := "GenTest"
	// columns, err := db.DescTableFunc(table)
	// if err != nil {
	// 	fmt.Printf("%v", err)
	// 	return
	// }
	// fmt.Println(generate.GeneratorStruct("gen_test", columns, true))
}

func TestEngine(t *testing.T) {
	g := generate.GeneratorEngine("localhost", "root", "123456", 3306, "test", "utf8mb4")
	g.Hump()
	g.SetTable("GenTest")
	val, err := g.GeneratorStruct()
	if err != nil {
		fmt.Printf("%v\n", err)
	} else {
		fmt.Println(val)
	}

}
