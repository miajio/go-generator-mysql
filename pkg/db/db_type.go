package db

import (
	"strings"

	"gitee.com/miajio/go-generator-mysql/pkg/common"
)

var (
	dbStrType   = []string{"longtext", "mediumtext", "text", "varchar"}
	dbByteType  = []string{"binary", "blob", "longblob", "mediumblob", "varbinary"}
	dbIntType   = []string{"bigint", "int", "integer", "smallint"}
	dbBoolType  = []string{"bool"}
	dbDateType  = []string{"date", "datetime"}
	dbFloatType = []string{"double", "float", "numeric"}

	dbIntTypeVal = []string{"int64", "int", "int8", "int32"}
)

func dbTypeToStructType(dbType string) string {
	if strings.Index(dbType, "(") > 0 {
		dbType = dbType[0:strings.Index(dbType, "(")]
	}

	if common.BinarySearch(dbStrType, dbType, strings.Compare) >= 0 {
		return "string"
	} else if common.BinarySearch(dbByteType, dbType, strings.Compare) >= 0 {
		return "[]byte"
	} else if common.BinarySearch(dbIntType, dbType, strings.Compare) >= 0 {
		return dbIntTypeVal[common.BinarySearch(dbIntType, dbType, strings.Compare)]
	} else if common.BinarySearch(dbBoolType, dbType, strings.Compare) >= 0 {
		return "bool"
	} else if common.BinarySearch(dbDateType, dbType, strings.Compare) >= 0 {
		return "time.Time"
	} else if common.BinarySearch(dbFloatType, dbType, strings.Compare) >= 0 {
		return "float"
	}
	return ""
}
