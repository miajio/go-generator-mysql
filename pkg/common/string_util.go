package common

import "strings"

type stringUtilImpl struct{}

type stringUtil interface {
	Hump(val string, upInitials bool) string // Hump 字符串转驼峰
	UpInitials(val string) string            // UpInitials 首字母大写
}

var StringUtil stringUtil = (*stringUtilImpl)(nil)

// Hump 字符串转驼峰
// val 需转换的字符串
// upInitials 首字母是否大写 -> 默认false
func (*stringUtilImpl) Hump(val string, upInitials bool) string {
	if val != "" {
		temp := strings.Split(val, "_")
		var result string
		for i := range temp {
			v := []rune(temp[i])
			if len(v) > 0 {
				if upInitials && v[0] >= 'a' && v[0] <= 'z' {
					v[0] -= 32
				}
				result += string(v)
			}
		}
		return result
	}
	return ""
}

// UpInitials 首字母大写
func (*stringUtilImpl) UpInitials(val string) string {
	if val == "" {
		return val
	} else if len(val) == 1 {
		return strings.ToUpper(val[0:1])
	}
	return strings.ToUpper(val[0:1]) + val[1:]
}
