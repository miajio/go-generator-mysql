package common_test

import (
	"fmt"
	"strings"
	"testing"

	"gitee.com/miajio/go-generator-mysql/pkg/common"
)

func TestBinarySearch(t *testing.T) {
	p := []string{"longtext", "text", "varchar"}

	fmt.Printf("binary search is: %d \n", common.BinarySearch(p, "text", strings.Compare))
}
